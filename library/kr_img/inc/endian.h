#pragma once

bool is_big_endian(void)
{
	union {
		unsigned int i;
		char c[4];
	} bint = { 0x01020304 };

	return bint.c[0] == 1;
}