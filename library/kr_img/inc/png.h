/*
     Dokumentacia k funkciam programu na otvorenie PNG obrazku.

	 Najpodstatnejsie funkcie su funkcie load_png a free_png.
	 Su to jedine funkcie, s ktorymi by mal 'uzivate' tejto kniznice
	 interagovat.
	 Vsetky zvysne funkcie v tomto subore sluzia iba ako pomocne funkcie pre load_png.

	 Pre lepsie porozumenie PNG formatu a jeho specifikaciam:
	 https://www.w3.org/TR/2003/REC-PNG-20031110/
*/

#pragma once
#define _CRT_SECURE_NO_WARNINGS		//	suppress warnings from cstdio
#include <cstdio>
#include "types.h"

/* ERROR LOGS */
#define READ_SUCCESS			0
#define READ_FAIL				1
#define FILE_NOT_OPENED			2
#define FILE_CORRUPTED			3
#define FILE_NONREADABLE		4


namespace png {

	/* Typ farby obrazka */
	enum color_t {
		grayscale = 0,
		truecolor = 2,
		indexed = 3,
		grayscale_alpha = 4,
		truecolor_alpha = 6
	};

	/*
	    Pouzivana ako node v double linked liste struktury chunk_list. Kazda jedna node obsahuje
		informacie o jednom chunku suboru ako su jeho pozicia od zaciatku suboru a velkost v bytoch.
	*/
	struct chunk_node {
		fpos_t chunk_start = 0;
		size_t chunk_size  = 0;
		chunk_node* next = nullptr;
		chunk_node* prev = nullptr;
	};

	/*
	    'Sentinel' pre zoznam odkazov na chunky jednoho typu. Ma odkaz na prvy a posledny chunk v
		liste a taktiez si uklada ich pocet.
		
		append vytvori novu node na heape a prida ju na koniec listu.
		
		free_all uvolni vsetky nodes z pamate.
	*/
	struct chunk_list {
		int count = 0;
		size_t data_size = 0;
		chunk_node* first = nullptr;
		chunk_node* last =  nullptr;

		inline int get_count() { return count; }
		inline size_t get_size() { return data_size; }

		void append(fpos_t data_start, size_t size);
		void free_all();
	};

	/*
	    Obsahuje zoznam pre kazdy typ chunkov, ktore sa mozu v obrazku vystihnut.

		read_all_chunks prejde subor, najde v nom vsetky chunky a popridava ich do listov.

		append_chunk prida jeden chunk na koniec listu jeho typu.

		free_all_chunks vycisti vsetky listy chunkov z pamate. Potom sa uz nedaju pouzit
	*/
	struct chunk_set {
		//	critical
		chunk_list IHDR;
		chunk_list PLTE;
		chunk_list IDAT;
		chunk_list IEND;
		//	transparency
		chunk_list tRNS;
		//	color space
		chunk_list cHRM;
		chunk_list gAMA;
		chunk_list iCCP;
		chunk_list sBIT;
		chunk_list sRGB;
		//	textual info
		chunk_list iTXt;
		chunk_list tEXt;
		chunk_list zTXt;
		//	misc info
		chunk_list bKGD;
		chunk_list hIST;
		chunk_list pHYs;
		chunk_list sPLT;
		//	time stamp
		chunk_list tIME;

		void append_chunk(byte* chunk, fpos_t data_start, size_t data_size);
		void read_all_chunks(FILE* file);
		void free_all_chunks();
	};

	/*
	    Abstraktna class sluziaca iba ako obal pre zakladne funkcie pouzivane pri nacitani
		PNG obrazku do pamate funkciou load_png.
	*/
	class util
	{
	public:
		/* Prevedie 16b farbu na 8b */
		static byte uint16_to_byte(uint16 value);

		/* Pretoci byty v poli medzi indexami start a end (vcitane) */
		static void reverse_bytes(byte* bytes, int start, int end);

		/*
		    Nacita jeden byte zo suboru file na index 3 pola buffer.
			Data, ktore boli predtym v tomto poli su posunute o jeden index dozadu,
			cize byte, ktory bol na nultom indexe je prepisany bytom s prveho indexu.
			Pokial sa nepodari zo suboru nacitat, tak je vratene false, inak true.
		*/
		static bool buffer_readto(byte* buffer, FILE* file);

		/* Porovna prve 4 byty pola buffer a str. Pokial su rovnake vrati true, inak false. */
		static bool buffer_cmp(byte* buffer, const char* str);

		/* Zisti, ci su byty v poli buff signature nejakeho z platnych chunkov. */
		static bool is_chunk(byte* buff);

		/* Paethov algoritmus na odfiltrovanie bytov. Lepsie ako dokumentacia je jeho implementacia. */
		static byte paeth(uint16 a, uint16 b, uint16 c);

		/* Vypocita aku velkost budu mat data obrazku po dekompresii. */
		static ulong get_uncompressed_size(uint width,
			uint height,
			byte bit_depth,
			byte channels_num,
			byte interlace);

		/*
		    Na vstupe dostane decompressed data obrazku a jeho parametre. Vrati obrazok ako
			jednorozmerne pole so zlozkami v poradi RGBA.
		*/
		static void* compose_image(byte* source,
			uint width,
			uint height,
			byte bit_depth,
			byte channels_num,
			byte interlace_method,
			byte color_type);

		/*
		    Volana z funkcie compose_image. Na vstupe dostane decompressed data obrazku a jeho
			parametre. Najprv tieto data odfiltruje, konvertuje ich na 8b zlozky pixelu a potom
			z nich vysklada obrazok. Obrazok je vrateny ako adresa jednorozmerneho pola so zlozkami
			v poradi RGBA.
		
		    Funguje iba pre non-interlaced obrazky.
		*/
		static void* compose_non_interlaced(byte* source,
			uint width,
			uint height,
			byte bit_depth,
			byte channels_num,
			byte color_type);

		/*
		    Verzia funkcie compose_non_interlaced pre interlaced obrazky.
		    Nieje implementovana.	
		*/
		static void* compose_interlaced(byte* source,
			uint width,
			uint height,
			byte bit_depth,
			byte channels_num,
			byte color_type);

		/*
		    Odfiltruje dany byte podla toho, ake su jeho susedne byty (before, above, before above)
			a filtrovacej metody.
			Na filtrovanie bytov sa pouziva 5 metod. Viac o nich v dokumentacii PNG.
		*/
		static uint16 unfilter(uint16 this_byte,
			uint16 byte_before,
			uint16 byte_above,
			uint16 byte_before_above,
			byte method);
	};

	/*
	    Precita prvych 8 bytov suboru a zisti, ci obsahuju PNG signature alebo nie.
		Pokial ide o PNG obrazok, tak vrati 0. V opacnom pripade vrati nenulove cislo.
	*/
	int read_SIGN(FILE* fileptr);

	/*
	    Precita IHDR chunk suboru a prepise jeho informacie do danych argumentov.
		Po uspechu vrati 0. V opacnom pripade vrati nenulove cislo 
		a posunie file iterator tam, kde bol pred spustenim funkcie.
	*/
	int read_IHDR(FILE* fileptr,
		uint* width,
		uint* height,
		byte* bit_depth,
		byte* color,
		byte* compression,
		byte* filter,
		byte* interlace);

	/*
		Precita IEND chunk suboru file. Pokial file iterator suboru fileptr neukazuje
		na zaciatok IEND chunku vrati nenulove cislo a posunie file iterator tam,
		kde bol pred spustenim funkcie. V opacnom pripade vrati 0.
	*/
	int read_IEND(FILE* fileptr);
}

/*
    Zakladna funkcia celej kniznice.
	Otvori subor 'filename' a overi, ci je to PNG obrazok.
	Pokial sa subor nepodari otvorit alebo pocas behu funkcie nastane hociaka ina chyba,
	tak return value bude nastavena nullptr.

	Argumenty width, height a channels budu nastavene na odpovedajuce udaje obrazku.

	Kazdy subor PNG sa sklada z viacerych 'chunkov', co su skupiny dat sluziace na rozne
	ucely(vyska, sirka alebo data obrazku). Je viacej typov chunkov, navzajom sa ich da odlisit
	podla ich 4 bytovej signature(napr. IHDR, IEND). Funkcia preskace po vsetkych chunkoch v subore a
	ulozi ich podstatne informacie do struktury 'chunk_set' (cize cas na prebehnutie
	suboru nieje linearny jeho velkosti, ale jeho poctu chunkov). Podstatnymi informaciami su myslene
	pozicia kazdeho chunku od zaciatku suboru a jeho velkost v bytoch. Samotne data chunkov sa zatial
	nikam neukladaju. Pokial je najdenych viac chunkov toho isteho typu, tak su za sebou radene do
	double linked listu. 

	Dalej otvorime hlavny chunk celeho PNG suboru, IHDR chunk. Tu sa nachadzaju informacie ako
	rozmery, bitova hlbka alebo typ farby v danom PNG obrazku.

	Funkcia dalej poziada o vytvorenie pola na heape pre compressed data obrazku. Tieto data su
	nasledne nacitane do pola a pomocou kniznice zlib odsifrovane do dalsieho, este vacsieho pola.
	Nasledne su data odfiltrovane a prevedene do formatu 1 byte na jednu zlozku/kanal pixelu.

	Podla poctu kanalov nacitaneho PNG obrazku su data konvertovane na 3 alebo 4 kanaly(iba v pripade,
	ze zdrojovy obrazok obsahuje alfa kanal). Adresa na tieto data je vratena v return hodnote.
	Pokial nastala pocas behu funkcie akakolvek chyba, tak je vrateny nullptr.
*/
void* load_png(const char* filename, int* width, int* height, int* channels, int* error = nullptr);

/*
    Ako argument pngdata sa dosadzuje return adresa funkcie load_png.
    Sluzi na vycistenie pamate obrazku v pamati. Po zavolani tejto funkcie sa nacitany obrazok uz
	neda zobrazit.
*/
void free_png(void* pngdata);