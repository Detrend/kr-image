#include "png.h"
#include "endian.h"
#include <zlib.h>
#include <cmath>

namespace png {

	int read_SIGN(FILE* fileptr)
	{
		if (fileptr == nullptr) {
			return FILE_NOT_OPENED;
		}

		const byte sign_bytes[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };
		byte bytes[8];
		size_t read = fread(bytes, sizeof(byte), 8, fileptr);

		if (read < 8) {
			return FILE_NONREADABLE;
		}

		for (int i = 0; i < 8; i++) {
			if (sign_bytes[i] != bytes[i]) {
				return READ_FAIL;
			}
		}

		return READ_SUCCESS;
	}

	int read_IHDR(FILE* fileptr,
		uint* width,
		uint* height,
		byte* bit_depth,
		byte* color,
		byte* compression,
		byte* filter,
		byte* interlace)
	{
		if (fileptr == nullptr) {
			return FILE_NOT_OPENED;
		}

		fpos_t read_start;
		fgetpos(fileptr, &read_start);

		size_t read;
		byte width_bytes[4];
		byte height_bytes[4];
		byte bit_depth_byte;
		byte color_type_byte;
		byte compression_byte;
		byte filter_byte;
		byte interlace_byte;
		byte validation_bytes[4];

		read = fread(width_bytes, sizeof(byte), 4, fileptr);
		if (read < 4) {
			fsetpos(fileptr, &read_start);
			return FILE_NONREADABLE;
		}

		read = fread(height_bytes, sizeof(byte), 4, fileptr);
		if (read < 4) {
			fsetpos(fileptr, &read_start);
			return FILE_NONREADABLE;
		}

		read = fread(&bit_depth_byte, sizeof(byte), 1, fileptr);
		if (read < 1) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		read = fread(&color_type_byte, sizeof(byte), 1, fileptr);
		if (read < 1) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		read = fread(&compression_byte, sizeof(byte), 1, fileptr);
		if (read < 1) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		read = fread(&filter_byte, sizeof(byte), 1, fileptr);
		if (read < 1) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		read = fread(&interlace_byte, sizeof(byte), 1, fileptr);
		if (read < 1) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		read = fread(validation_bytes, sizeof(byte), 4, fileptr);
		if (read < 4) {
			fsetpos(fileptr, &read_start); 
			return FILE_NONREADABLE;
		}

		if (!is_big_endian()) {
			util::reverse_bytes(width_bytes, 0, 3);
			util::reverse_bytes(height_bytes, 0, 3);
			util::reverse_bytes(validation_bytes, 0, 3);
		}

		if (width)
			*width = *((uint*)width_bytes);

		if (height)
			*height = *((uint*)height_bytes);

		if (bit_depth)
			*bit_depth = bit_depth_byte;

		if (color)
			*color = color_type_byte;

		if (compression)
			*compression = compression_byte;

		if (filter)
			*filter = filter_byte;

		if (interlace)
			*interlace = interlace_byte;

		return READ_SUCCESS;
	}

	void chunk_list::append(fpos_t data_start, size_t size)
	{
		chunk_node* node = new chunk_node;

		node->chunk_start = data_start;
		node->chunk_size = size;

		count += 1;
		data_size += size;

		node->prev = last;
		node->next = nullptr;
		if (first == nullptr) {
			first = node;
		}
		if (last == nullptr) {
			last = node;
		} else {
			last->next = node;
			last = node;
		}
	}

	void chunk_list::free_all() {
		chunk_node* it = first;
		while (it != nullptr) {
			chunk_node* nxt = it->next;
			delete it;
			it = nxt;
		}
	}

	void chunk_set::append_chunk(byte* chunk, fpos_t data_start, size_t data_size)
	{
		if (util::buffer_cmp(chunk, "IHDR"))
			IHDR.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "PLTE"))
			PLTE.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "IDAT"))
			IDAT.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "IEND"))
			IEND.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "tRNS"))
			tRNS.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "cHRM"))
			cHRM.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "gAMA"))
			gAMA.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "iCCP"))
			iCCP.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "sBIT"))
			sBIT.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "sRGB"))
			sRGB.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "iTXt"))
			iTXt.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "tEXt"))
			tEXt.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "zTXt"))
			zTXt.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "bKGD"))
			bKGD.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "hIST"))
			hIST.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "pHYs"))
			pHYs.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "sPLT"))
			sPLT.append(data_start, data_size);
		if (util::buffer_cmp(chunk, "tIME"))
			tIME.append(data_start, data_size);
	}

	void chunk_set::read_all_chunks(FILE* file)
	{
		if (file == nullptr) {
			return;
		}

		fpos_t read_start;
		fgetpos(file, &read_start);	//	jump back here once we are done

		bool big_endian = is_big_endian();

		byte buffer[4];
		bool stop = false;

		fread(buffer, 1, 4, file);
		while (!stop) {
			if (util::is_chunk(buffer)) {
				byte length_bytes[4];
				fpos_t data_start;
				size_t data_size;

				fgetpos(file, &data_start);
				fseek(file, -8, SEEK_CUR);

				fread(length_bytes, 1, 4, file);
				if (!big_endian) {
					util::reverse_bytes(length_bytes, 0, 3);
				}
				data_size = *((unsigned int*)length_bytes);
				fseek(file, static_cast<long>(data_size + 8), SEEK_CUR);

				append_chunk(buffer, data_start, data_size);
			}

			bool res = util::buffer_readto(buffer, file);
			if (res == false) {
				stop = true;
			}
		}

	}

	void chunk_set::free_all_chunks()
	{
		IHDR.free_all();
		PLTE.free_all();
		IDAT.free_all();
		IEND.free_all();
		tRNS.free_all();
		cHRM.free_all();
		gAMA.free_all();
		iCCP.free_all();
		sBIT.free_all();
		sRGB.free_all();
		iTXt.free_all();
		tEXt.free_all();
		zTXt.free_all();
		bKGD.free_all();
		hIST.free_all();
		pHYs.free_all();
		sPLT.free_all();
	}

	int read_PLTE(FILE* file, uint* length)
	{
		if (file == nullptr) {
			return FILE_NOT_OPENED;
		}

		fpos_t read_start;
		fgetpos(file, &read_start);

		byte length_bytes[4];
		byte PLTE_bytes[4];
		byte validation_bytes[4];
		size_t read;

		read = fread(length_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		read = fread(PLTE_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		const byte PLTE[4] = { 'P', 'L', 'T', 'E' };
		for (int i = 0; i < 4; i++) {
			if (PLTE[i] != PLTE_bytes[i]) {
				fsetpos(file, &read_start);
				return READ_FAIL;
			}
		}

		read = fread(validation_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		if (!is_big_endian()) {
			util::reverse_bytes(length_bytes, 0, 3);
			util::reverse_bytes(validation_bytes, 0, 3);
		}

		unsigned int len = *((unsigned int*)length_bytes);
		if (length)
			*length = len;

		return READ_SUCCESS;
	}

	int read_IDAT(FILE* file, uint* length)
	{
		if (file == nullptr) {
			return FILE_NOT_OPENED;
		}

		fpos_t read_start;
		fgetpos(file, &read_start);

		byte length_bytes[4];
		byte IDAT_bytes[4];
		byte validation_bytes[4];
		size_t read;

		read = fread(length_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		read = fread(IDAT_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		const byte IDAT[4] = { 'I', 'D', 'A', 'T' };
		for (int i = 0; i < 4; i++) {
			if (IDAT[i] != IDAT_bytes[i]) {
				fsetpos(file, &read_start);
				return READ_FAIL;
			}
		}

		read = fread(validation_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		if (!is_big_endian()) {
			util::reverse_bytes(length_bytes, 0, 3);
			util::reverse_bytes(validation_bytes, 0, 3);
		}

		unsigned int len = *((unsigned int*)length_bytes);
		unsigned int val = *((unsigned int*)validation_bytes);

		if (length)
			*length = len;

		return READ_SUCCESS;
	}

	int read_IEND(FILE* file)
	{
		if (file == nullptr) {
			return FILE_NOT_OPENED;
		}

		fpos_t read_start;
		fgetpos(file, &read_start);

		byte length_bytes[4];
		byte IEND_bytes[4];
		byte validation_bytes[4];
		size_t read;

		read = fread(length_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		read = fread(IEND_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		const byte IEND[4] = { 'I', 'E', 'N', 'D' };
		for (int i = 0; i < 4; i++) {
			if (IEND[i] != IEND_bytes[i]) {
				fsetpos(file, &read_start);
				return READ_FAIL;
			}
		}

		read = fread(validation_bytes, sizeof(byte), 4, file);
		if (read < 4) {
			fsetpos(file, &read_start);
			return FILE_NONREADABLE;
		}

		if (!is_big_endian()) {
			util::reverse_bytes(length_bytes, 0, 3);
			util::reverse_bytes(validation_bytes, 0, 3);
		}

		//	check bytes

		return READ_SUCCESS;
	}

	byte util::uint16_to_byte(uint16 value) {
		return static_cast<byte>((byte)-1 * ((double)value / (uint16)-1));
	}

	void util::reverse_bytes(byte* bytes, int start, int end) {
		while (start < end) {
			byte temp = bytes[start];
			bytes[start++] = bytes[end];
			bytes[end--] = temp;
		}
	}

	bool util::buffer_readto(byte* buffer, FILE* file)
	{
		if (buffer == nullptr)
			return false;

		for (int i = 1; i < 4; i++) {
			buffer[i - 1] = buffer[i];
		}

		byte read;
		size_t count = fread(&read, 1, 1, file);
		buffer[3] = read;
		if (count > 0)
			return true;
		else
			return false;
	}

	bool util::buffer_cmp(byte* buffer, const char* str)
	{
		if (buffer == nullptr or str == nullptr)
			return false;

		for (int i = 0; i < 4; i++)
			if (buffer[i] != (byte)(str[i]))
				return false;

		return true;
	}

	bool util::is_chunk(byte* buff)
	{
		if (buff == nullptr)
			return false;
		if (buffer_cmp(buff, "IHDR"))
			return true;
		if (buffer_cmp(buff, "PLTE"))
			return true;
		if (buffer_cmp(buff, "IDAT"))
			return true;
		if (buffer_cmp(buff, "IEND"))
			return true;
		if (buffer_cmp(buff, "tRNS"))
			return true;
		if (buffer_cmp(buff, "cHRM"))
			return true;
		if (buffer_cmp(buff, "gAMA"))
			return true;
		if (buffer_cmp(buff, "iCCP"))
			return true;
		if (buffer_cmp(buff, "sBIT"))
			return true;
		if (buffer_cmp(buff, "sRGB"))
			return true;
		if (buffer_cmp(buff, "iTXt"))
			return true;
		if (buffer_cmp(buff, "tEXt"))
			return true;
		if (buffer_cmp(buff, "zTXt"))
			return true;
		if (buffer_cmp(buff, "bKGD"))
			return true;
		if (buffer_cmp(buff, "hIST"))
			return true;
		if (buffer_cmp(buff, "pHYs"))
			return true;
		if (buffer_cmp(buff, "sPLT"))
			return true;
		if (buffer_cmp(buff, "tIME"))
			return true;
		return false;
	}

	byte util::paeth(uint16 a, uint16 b, uint16 c)
	{
		int p = a + b - c;
		int pa = abs(p - a);
		int pb = abs(p - b);
		int pc = abs(p - c);
		int pr;

		if (pa <= pb and pa <= pc) {
			pr = a;
		} else if (pb <= pc) {
			pr = b;
		} else {
			pr = c;
		}

		return pr;
	}

	uint16 util::unfilter(uint16 this_byte,
		uint16 byte_before,
		uint16 byte_above,
		uint16 byte_before_above,
		byte method)
	{
		switch (method) {
		case 0: break;
		case 1:
			this_byte = this_byte + byte_before;
			break;
		case 2:
			this_byte = this_byte + byte_above;
			break;
		case 3:
			this_byte = this_byte + static_cast<uint16>(floor((byte_before + byte_above) / 2.0));
			break;
		case 4:
			this_byte = this_byte + paeth(byte_before, byte_above, byte_before_above);
			break;
		default:
			__debugbreak();
			break;
		}

		return this_byte;
	}

	ulong util::get_uncompressed_size(uint width,
		uint height,
		byte bit_depth,
		byte channels_num,
		byte interlace)
	{
		ulong size = 0;

		switch (interlace) {
		case 0:
			size = static_cast<ulong>(ceil(channels_num * width * height * bit_depth / 8.0) + height);
			break;
		case 1:
			size = static_cast<ulong>(ceil(channels_num * width * height * bit_depth / 8.0));
			size += (height + 7) / 8;			//	interlace layer 1
			size += (height + 7) / 8;			//	interlace layer 2
			size += (height + 3) / 8;			//	interlace layer 3
			size += (height + 3) / 4;			//	interlace layer 4
			size += (height + 1) / 4;			//	interlace layer 5
			size += (height + 1) / 2;			//	interlace layer 6
			size += (height) / 2;			//	interlace layer 7
			size += 8;
			break;
		default:
			//	error
			break;
		}

		return size;
	}

	void* util::compose_non_interlaced(byte* source,
		uint width,
		uint height,
		byte bit_depth,
		byte channels_num,
		byte color_type)
	{
		uint byte_offset = static_cast<uint>(channels_num * ceil(bit_depth / 8.0));
		uint scanline_len = static_cast<uint>(ceil(width * channels_num * bit_depth / 8.0) + 1);
		uint channels = color_type == 0 or color_type == 2 ? 3 : 4;

		uint image_data_size = width * height * channels;
		byte* image_data = new(std::nothrow) byte[image_data_size];
		if (image_data == nullptr) {
			__debugbreak();
			return nullptr;
		}

		for (uint row = 0; row < height; row++) {
			byte scanline_filter = source[row * scanline_len];

			for (uint col = 1; col < scanline_len; col++) {
				byte& target_byte = source[row * scanline_len + col];
				byte before = 0, above = 0, before_above = 0;

				if (col > byte_offset and row > 0) {
					before = source[row * scanline_len + col - byte_offset];
					above = source[(row - 1) * scanline_len + col];
					before_above = source[(row - 1) * scanline_len + col - byte_offset];
				} else if (col > byte_offset) {
					before = source[row * scanline_len + col - byte_offset];
				} else if (row > 0) {
					above = source[(row - 1) * scanline_len + col];
				}

				target_byte = static_cast<byte>(unfilter(target_byte, before, above, before_above, scanline_filter));

				if (color_type == 0) {
					if (bit_depth == 1 or bit_depth == 2 or bit_depth == 4) {
						uint rep = 8 / bit_depth;
						uint pos = row * width * 3 + (col - 1) * 3 * rep;
						byte data = target_byte;
						byte base_mask = 0xF >> (4 - bit_depth);
						for (uint i = 0; i < rep; i++) {
							byte mask = base_mask << (i * bit_depth);
							byte val = static_cast<byte>((byte)((data & mask) >> (i * bit_depth)) * (255 / (float)base_mask));
							for (int j = 0; j < 3; j++) {
								image_data[pos + (rep - 1 - i) * 3 + j] = val;
							}
						}
					} else if (bit_depth == 8) {
						uint pos = row * width * 3 + (col - 1) * 3;
						image_data[pos] = target_byte;
						image_data[pos + 1] = target_byte;
						image_data[pos + 2] = target_byte;
					} else if (bit_depth == 16) {
						if (col % 2 == 0) {
							uint pos = row * width * 3 + ((col / 2) - 1) * 3;

							byte red[2] = { source[row * scanline_len + col - 1], source[row * scanline_len + col] };
							if (!is_big_endian()) {
								reverse_bytes(red, 0, 1);
							}

							byte red_conv = uint16_to_byte(*((uint16*)red));
							image_data[pos] = red_conv;
							image_data[pos + 1] = red_conv;
							image_data[pos + 2] = red_conv;
						}
					}
				} else if (color_type == 2 or color_type == 6) {		//	(truecolor) & (truecolor + alpha)
					if (bit_depth == 8)
					{
						uint pos = row * width * channels + (col - 1);
						image_data[pos] = target_byte;
					} else if (bit_depth == 16)
					{
						if (col % 2 == 0)
						{
							uint pos = row * width * channels + (col / 2) - 1;
							byte bytes[2] = { source[row * scanline_len + col - 1], source[row * scanline_len + col] };

							if (!is_big_endian()) {
								reverse_bytes(bytes, 0, 1);
							}

							uint16 sample = *((uint16*)bytes);
							image_data[pos] = uint16_to_byte(sample);
						}
					}
				} else if (color_type == 4)					//	(grayscale + alpha)		
				{
					if (bit_depth == 8)
					{
						if (col % 2 == 0)
						{
							uint pos = row * width * 4 + (col - 2) * 2;
							byte red = source[row * scanline_len + col - 1];
							byte alpha = source[row * scanline_len + col];
							image_data[pos] = red;
							image_data[pos + 1] = red;
							image_data[pos + 2] = red;
							image_data[pos + 3] = alpha;
						}
					} else if (bit_depth == 16)
					{
						if (col % 4 == 0) {
							uint pos = row * width * 4 + col - 4;
							byte red[2] = { source[row * scanline_len + col - 3], source[row * scanline_len + col - 2] };
							byte alpha[2] = { source[row * scanline_len + col - 1], source[row * scanline_len + col] };

							if (!is_big_endian()) {
								reverse_bytes(red, 0, 1);
								reverse_bytes(alpha, 0, 1);
							}

							byte red_converted = uint16_to_byte(*((uint16*)red));
							byte alpha_converted = uint16_to_byte(*((uint16*)alpha));

							image_data[pos] = red_converted;
							image_data[pos + 1] = red_converted;
							image_data[pos + 2] = red_converted;
							image_data[pos + 3] = alpha_converted;
						}
					}
				} else
				{								
					__debugbreak();
					delete[] image_data;
					return nullptr;
				}

			}
		}

		return image_data;
	}

	void* util::compose_interlaced(byte* source,
		uint width,
		uint height,
		byte bit_depth,
		byte channels_num,
		byte color_type)
	{
		return nullptr;
	}

	void* util::compose_image(byte* source,
		uint width,
		uint height,
		byte bit_depth,
		byte channels_num,
		byte interlace_method,
		byte color_type)
	{
		if (interlace_method == 0) {
			return compose_non_interlaced(source, width, height, bit_depth, channels_num, color_type);
		} else if (interlace_method == 1) {
			return compose_interlaced(source, width, height, bit_depth, channels_num, color_type);
		} else {
			__debugbreak();
		}

		return nullptr;
	}
}

void* load_png(const char* filename, int* arg_width, int* arg_height, int* arg_channels, int* arg_error)
{
	using namespace png;

	FILE* file = fopen(filename, "rb");

	if (file == nullptr) {
		__debugbreak();
		return nullptr;
	}
	int fret;
	fret = read_SIGN(file);

	if (fret != READ_SUCCESS) {
		__debugbreak();
		fclose(file);
		return nullptr;
	}

	chunk_set chunks;
	chunks.read_all_chunks(file);

	if (chunks.IHDR.count != 1 or chunks.IDAT.count <= 0 or chunks.IEND.count != 1) {
		__debugbreak();
		fclose(file);
		chunks.free_all_chunks();
		return nullptr;
	}

	size_t compressed_data_size;
	byte*  compressed_data;

	size_t decompressed_data_size;
	byte*  decompressed_data;

	size_t unfiltered_data_size;
	byte*  unfiltered_data;

	uint width;					//	width of the image
	uint height;				//	height of the image
	byte bit_depth;				//	number of bits per sample
	byte color_type;			//	type of color
	byte compression_method;
	byte filter_method;
	byte interlace_method;
	byte sample_depth;
	byte channels_num;			//	number of image channels
	uint bits_per_sample;		//	

	fsetpos(file, &chunks.IHDR.first->chunk_start);

	fret = read_IHDR(file,
		&width,
		&height,
		&bit_depth,
		&color_type,
		&compression_method,
		&filter_method,
		&interlace_method);

	if (fret != READ_SUCCESS or filter_method != 0) {
		__debugbreak();
		fclose(file);
		chunks.free_all_chunks();
		return nullptr;
	}

	switch (color_type) {
	case grayscale:
		channels_num = 1;
		sample_depth = bit_depth;
		break;
	case truecolor:
		channels_num = 3;
		sample_depth = bit_depth;
		break;
	case indexed:
		channels_num = 1;
		sample_depth = 8;
		break;
	case grayscale_alpha:
		channels_num = 2;
		sample_depth = bit_depth;
		break;
	case truecolor_alpha:
		channels_num = 4;
		sample_depth = bit_depth;
		break;
	}

	compressed_data_size = chunks.IDAT.get_size();
	compressed_data = new(std::nothrow) byte[compressed_data_size];
	if (compressed_data == nullptr) {
		__debugbreak();
		fclose(file);
		chunks.free_all_chunks();
		return nullptr;
	}

	uint compressed_data_iterator = 0;
	chunk_node* node_iterator = chunks.IDAT.first;
	while (node_iterator != nullptr) {
		const size_t& read_size = node_iterator->chunk_size;
		const fpos_t& read_start = node_iterator->chunk_start;
		size_t read_bytes;

		fsetpos(file, &read_start);
		read_bytes = fread(compressed_data + compressed_data_iterator, 1, read_size, file);
		compressed_data_iterator += static_cast<uint>(read_size);
		if (read_bytes != read_size) {
			__debugbreak();
			fclose(file);
			delete[] compressed_data;
			chunks.free_all_chunks();
			return nullptr;
		}

		node_iterator = node_iterator->next;
	}
	if (compressed_data_iterator > compressed_data_size) {
		__debugbreak();
		fclose(file);
		delete[] compressed_data;
		chunks.free_all_chunks();
		return nullptr;
	}
	
	bits_per_sample = sample_depth;
	decompressed_data_size = util::get_uncompressed_size(width, height, bits_per_sample, channels_num, interlace_method);
	decompressed_data = new(std::nothrow) byte[decompressed_data_size];
	if (decompressed_data == nullptr) {
		__debugbreak();
		fclose(file);
		delete[] compressed_data;
		chunks.free_all_chunks();
		return nullptr;
	}

	ulong decompressed_data_real_size = static_cast<ulong>(decompressed_data_size);
	fret = uncompress(decompressed_data, &decompressed_data_real_size, compressed_data, (ulong)compressed_data_size);
	if (decompressed_data_size < decompressed_data_real_size) {
		__debugbreak();
		fclose(file);
		delete[] compressed_data;
		delete[] decompressed_data;
		chunks.free_all_chunks();
		return nullptr;
	}
	switch (fret) {
	case Z_OK:
		//	everything ok
		break;
	default:
		__debugbreak();
		fclose(file);
		delete[] compressed_data;
		delete[] decompressed_data;
		chunks.free_all_chunks();
		return nullptr;
		break;
	}

	delete[] compressed_data;

	unfiltered_data_size = decompressed_data_size;
	unfiltered_data = new(std::nothrow) byte[unfiltered_data_size];
	if (unfiltered_data == nullptr) {
		__debugbreak();
		fclose(file);
		chunks.free_all_chunks();
		return nullptr;
	}

 	void* composed_image_data = util::compose_image(decompressed_data, width, height, bit_depth, channels_num, interlace_method, color_type);

	delete[] decompressed_data;

	if (color_type == 4) {
		channels_num = 4;
	} else if (color_type == 0) {
		channels_num = 3;
	}

	if (arg_width)
		*arg_width = width;

	if (arg_height)
		*arg_height = height;

	if (arg_channels)
		*arg_channels = channels_num;

	chunks.free_all_chunks();
	fclose(file);

	return composed_image_data;
}

void free_png(void* pngdata)
{
	delete[] pngdata;
}