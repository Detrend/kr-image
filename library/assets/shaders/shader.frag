#version 330 core
out vec4 FragColor;

in vec2 fTexPos;
uniform sampler2D ImgTexture;

vec4 backCol = vec4(0.3f, 0.3f, 0.3f, 1.0f);

void main()
{
	vec4 texCol = texture(ImgTexture, fTexPos);
	FragColor = mix(backCol, texCol, texCol.a);
}