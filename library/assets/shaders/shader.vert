#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 tPos;

uniform vec3 vScreenParams;

out vec2 fTexPos;

void main()
{
	fTexPos = tPos;
    gl_Position = vec4(aPos.x * vScreenParams.x * vScreenParams.y, aPos.y, aPos.z, 1.0);
}