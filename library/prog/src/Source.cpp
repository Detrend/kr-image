#include "Include.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

void error_box(const char* caption, const char* text)
{
	MessageBox(NULL, text, caption, MB_OK);
}

GLFWwindow* create_window(const char* name)
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(640, 480, name, NULL, NULL);
	if (window == nullptr)
	{
		std::cout << "GLFW window error" << std::endl;
		glfwTerminate();
		return nullptr;
	}

	glfwMakeContextCurrent(window);

	return window;
}
