#pragma once

#include <string>
#include <sstream>
#include <fstream>

#include <glew.h>

/* Nacita, zkompiluje a zlinkuje vertex a fragment shadery z ich suborov. Vracia OpenGL
   shader program handle */
GLuint CreateShader(const char* vertex_path, const char* fragment_path);