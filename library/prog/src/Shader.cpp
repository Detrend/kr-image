#include "Shader.h"
#include <iostream>

GLuint CreateShader(const char* VertexPath, const char* FragmentPath)
{
	std::string VertexSource;	//	sem nacitame zdrojak vertex shaderu ako string

	std::ifstream VertexFile;
	std::string line;
	VertexFile.open(VertexPath);

	if (!VertexFile.is_open()) {
		__debugbreak();
	}

	while (std::getline(VertexFile, line)) {
		VertexSource += std::string("\n") + line;
	}

	VertexFile.close();

	std::string FragmentSource;	//	sem nacitame zdrojak fragment shaderu ako string
	std::ifstream FragmentFile;
	FragmentFile.open(FragmentPath);

	if (!FragmentFile.is_open()) {
		__debugbreak();
	}

	while (std::getline(FragmentFile, line)) {
		FragmentSource += std::string("\n") + line;
	}

	FragmentFile.close();

	int succ;				//	success?
	char info[512];			//	sem nacitame info log, pokial sa nieco pokazi

	GLuint VertexHandle = glCreateShader(GL_VERTEX_SHADER);
	const char* VCharS = VertexSource.c_str();

	/* Kompilacia vertex shaderu */
	glShaderSource(VertexHandle, 1, &VCharS, nullptr);
	glCompileShader(VertexHandle);
	glGetShaderiv(VertexHandle, GL_COMPILE_STATUS, &succ);
	if (!succ) {
		glGetShaderInfoLog(VertexHandle, 512, nullptr, info);
		std::cout << info << std::endl;
		__debugbreak();
	}

	GLuint FragmentHandle = glCreateShader(GL_FRAGMENT_SHADER);
	const char* FCharS = FragmentSource.c_str();

	/* Kompilacia fragment shaderu */
	glShaderSource(FragmentHandle, 1, &FCharS, nullptr);
	glCompileShader(FragmentHandle);
	glGetShaderiv(FragmentHandle, GL_COMPILE_STATUS, &succ);
	if (!succ) {
		glGetShaderInfoLog(FragmentHandle, 512, nullptr, info);
		std::cout << info << std::endl;
		__debugbreak();
	}

	/* Linkneme vertex a fragment shadery a vytvorime z nich GPU program */
	GLuint ProgramHandle = glCreateProgram();
	glAttachShader(ProgramHandle, VertexHandle);
	glAttachShader(ProgramHandle, FragmentHandle);
	glLinkProgram(ProgramHandle);
	glGetProgramiv(ProgramHandle, GL_LINK_STATUS, &succ);
	if (!succ) {
		glGetProgramInfoLog(ProgramHandle, 512, nullptr, info);
		std::cout << info << std::endl;
		__debugbreak();
	}

	/* Shadery sme uz linkli a vytvorili z nich GPU program, teraz sa ich mozeme zbavit */
	glDeleteShader(FragmentHandle);
	glDeleteShader(VertexHandle);

	/* Vratime handle na shader program, ktory sme vytvorili */
	return ProgramHandle;
}