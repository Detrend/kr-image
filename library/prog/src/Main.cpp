/*   ***************************************************************************

     Zdrojak k zapoctovemu programu z predmetu programovanie. 
     Program sluzi na zobrazovanie PNG obrazkov.

	 Matus R. 2020/2021
	 
	 Podporovane su PNG obrazky typu grayscale, truecolor, truecolor with alpha
	 a grayscale with alpha. Obrazky typu indexed alebo interlaced niesu
	 podporovane.

	 Dokumentacia funkcii load_png a free_png, ktore sluzia na otvaranie a
	 nacitanie obrazkov do pamate sa nachadza v subore png.h.

     **************************************************************************/

#include "Include.h"

static int WinWidth = 1920;
static int WinHeight = 1080;

/* Zavolana pri kazdej zmene velkosti okna. Stara sa o to, aby OpenGL vykreslovalo na cele okno */
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	WinWidth = width;
	WinHeight = height;
	glViewport(0, 0, width, height);
}

void ClearErrors() {
	GLenum err;
	do {
		err = glGetError();
	} while (err != GL_NO_ERROR);
}

#define PrintError(err) std::cout << #err;

void GetErrors() {
	GLenum err = glGetError();
	switch (err) {
	case GL_NO_ERROR:
		PrintError(GL_NO_ERROR);
		break;
	case GL_INVALID_ENUM:
		PrintError(GL_INVALID_ENUM);
		break;
	case GL_INVALID_OPERATION:
		PrintError(GL_INVALID_OPERATION);
		break;
	case GL_INVALID_FRAMEBUFFER_OPERATION:
		PrintError(GL_INVALID_FRAMEBUFFER_OPERATION);
		break;
	case GL_OUT_OF_MEMORY:
		PrintError(GL_OUT_OF_MEMORY);
		break;
	case GL_STACK_UNDERFLOW:
		PrintError(GL_STACK_UNDERFLOW);
		break;
	case GL_STACK_OVERFLOW:
		PrintError(GL_STACK_OVERFLOW);
		break;
	default:
		PrintError(DEFAULT);
		break;
	}
	std::cout << std::endl;
}

int main() {

	/* Inicilizacia kniznic na tvorbu okien */
	if (glfwInit() == GLFW_FALSE) {
		error_box("GLFW error", "Failed to initialize GLFW");
		return 1;
	}

	/* Vytvorenie okna */
	GLFWwindow* window = create_window("Image viewer");
	if (window == nullptr) {
		error_box("Window creation error", "Failed to create window");
		glfwTerminate();
		return 2;
	}

	/* GLEW nam poskytuje vsetky OpenGL funkcie */
	if (glewInit() != GLEW_OK) {
		error_box("GLEW error", "Failed to initialize GLEW");
		glfwTerminate();
		return 3;
	}

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	framebuffer_size_callback(window, 1920, 1080);
	glfwMaximizeWindow(window);

	std::fstream InputFile;
	InputFile.open("input.txt", std::fstream::in);

	std::string FilePath;
	if (!InputFile.is_open()) {
		error_box("input.txt not found", "Make sure there is an input.txt file in folder with program.");
		glfwTerminate();
		return 3;
	} else {
		std::getline(InputFile, FilePath);
	}

	InputFile.close();

	/* Suradnice trojuholnikov, na ktore OpenGL vykresli obrazok */
	float vertices[] = { 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
						-0.5f, 0.5f, 0.0f, 0.0f, 0.0f,
						-0.5f,-0.5f, 0.0f, 0.0f, 1.0f,
						 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,
						-0.5f,-0.5f, 0.0f, 0.0f, 1.0f,
						 0.5f,-0.5f, 0.0f, 1.0f, 1.0f };

	/* Cez Vertex Buffer Objekt nahrame data z 'vertices' do GPU */
	GLuint VBO;
	glGenBuffers(1, &VBO);

	/* OpenGL vyzaduje jeden vytvoreny Vertex Array Objekt, inak nefunguje.
	   K nicomu ho nepouzijeme */
	GLuint VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	/* Data z 'vertices' nalejeme do GFX */
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	/* Vysvetlime GFX ako ma data, ktore sme jej dali interpretovat.
	   Prve 3 floating point hodnoty su suradnice polohy, dalsie 2 suradnice textury(obrazku) */
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	ClearErrors();
	/* Nacitame zo suborov zdrojaky shaderov, skompilujeme a linkneme ich */
	GLuint ShaderProgram = CreateShader("assets/shaders/shader.vert", "assets/shaders/shader.frag");
	glUseProgram(ShaderProgram);
	GetErrors();

	int x, y, c;
	/* Nahrame obrazok do pamate */
	void* img_data = load_png(FilePath.c_str(), &x, &y, &c);
	//void* img_data = stbi_load(FilePath.c_str(), &x, &y, &c, 4);	//	na testovanie

	if (img_data == nullptr) {
		error_box("Image load error", "failed to load the image");
		glfwTerminate();
		return 4;
	}
	
	/* Vytvorime objekt pre texturu v OpenGL a jeho id ulozime do 'Texture' */
	GLuint Texture;
	glGenTextures(1, &Texture);
	glBindTexture(GL_TEXTURE_2D, Texture);

	/* Nalejeme data obrazku z RAM do GFX a pritom vysvetlime OpenGL ako ich ma interpretovat
	   (pocet kanalov, vyska, sirka, bit depth)*/
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, x, y, 0, c == 3 ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, img_data);

	/* Dame vediet OpenGL, ze nechceme aby pixely v obrazkoch s nizkym
	   rozlisenim rozmazavalo */
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, Texture);

	/* Dame shaderu vediet pomer stran nasej obrazovky, aby vykreslil obrazok ako stvorec */
	int ScreenParamsUniLoc = glGetUniformLocation(ShaderProgram, "vScreenParams");
	float m = (float)WinHeight / WinWidth;
	float n = (float)x / y;
	glUseProgram(ShaderProgram);

	/* Loop, dokola vykreslujeme obrazok */
	while (!glfwWindowShouldClose(window)) {
		m = (float)WinHeight / WinWidth;
		n = (float)x / (float)y;
		glUniform3f(ScreenParamsUniLoc, m, n, 0.0f);

		glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	free_png(img_data);
	glfwTerminate();
	return 0;
}