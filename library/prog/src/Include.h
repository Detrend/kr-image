#pragma once

#include <Windows.h>

#define GLEW_STATIC
#include <glew.h>
#include <glfw3.h>

#include <stb_image.h>

#include "kr_img.h"
#include <iostream>
#include <sstream>
#include <fstream>

#include "Shader.h"

void error_box(const char* caption, const char* text);

GLFWwindow* create_window(const char* name);